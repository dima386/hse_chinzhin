if (window.location.pathname === '/') {
    sendRequest('GET', "/api/?id=6")
        .then(
            data => showPersons(data))
}

function sendRequest(method, url, body = null) {

    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()

        xhr.open(method, url);
        xhr.responseType = 'json';
        xhr.onload = () => {
            if (xhr.status >= 400) {
                reject(xhr.response)
            } else {
                resolve(xhr.response)
            }
        }
        xhr.send(JSON.stringify(body));
    })
};

function showPersons(data) {
    let tr
    let table = document.getElementById('table');
    for (let i = 0; i < data.length; i++) {
        tr = document.createElement('tr');
        tr.setAttribute("id", data[i].id);
        tr.innerHTML = `
            <td>${data[i].name}</td>
            <td class="birthday"></td>
            <td> <button onclick=getBirthday(this) type="button" class="show__button btn btn-primary">Посмотреть дату</button>  </td>
            <td> <button onclick=changeBirthday(this) type="button" class="change__button btn btn-secondary">Изменить</button>  </td>
            `
        table.appendChild(tr);
    }
}

function getBirthday(td) {
    let tr = td.parentElement.parentElement;
    sendRequest('GET', `/api/user/?birthday=${tr.id}`)
        .then(
            data => showBirthday(data, tr))
}

function showBirthday(data, button) {
    birthday = button.getElementsByClassName('birthday');
    btn = button.getElementsByClassName('change__button');
    btn[0].innerText = 'Изменить';
    btn[0].setAttribute("class", "change__button btn btn-secondary");
    birthday[0].innerText = data[0].birthday;
}

function changeBirthday(td) {
    let tr = td.parentElement.parentElement;
    if (td.innerText === 'Изменить') {

        sendRequest('GET', `/api/user/?birthday=${tr.id}`)
            .then(
                data => showBirthdayInput(data, tr))
    } else {
        acceptBirthday(td, tr.id);

    }
}

function showBirthdayInput(data, button) {
    birthday = button.getElementsByClassName('birthday');
    btn = button.getElementsByClassName('change__button');
    btn[0].innerText = 'Сохранить';
    btn[0].setAttribute("class", "change__button btn btn-danger");

    inputBirthday = document.createElement('input');
    inputBirthday.setAttribute("value", data[0].birthday);
    inputBirthday.setAttribute("name", "birthday");
    inputBirthday.setAttribute("placeholder", "Дата рождения");
    birthday[0].innerText = ''
    birthday[0].appendChild(inputBirthday);
}

function acceptBirthday(td, id) {
    let tr = td.parentElement.parentElement;
    td.innerText = 'Изменить';
    td.setAttribute("class", "change__button btn btn-secondary");
    birthdayB = tr.querySelectorAll('input');
    birthday = birthdayB[0].value;
    const body = {
        id: id,
        birthday: birthday,
    };
    sendRequest('POST', '/api/birthday/', body)
        .then(getBirthday(td));



}