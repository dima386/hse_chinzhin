<?php

$db = mysqli_connect('localhost', 'root', '', 'test_hserm'); 

$sql_array = array (
    '1' => "SELECT DISTINCT persons.id, persons.name, request.user_id, request.id AS request_id
            FROM persons 
            JOIN request ON persons.id = request.user_id;",

    '2' => "SELECT `id`, `name`, `birthday` FROM persons WHERE YEAR(birthday) = 1990;",

    '3' => "SELECT DISTINCT persons.id, persons.name, request.id AS request_id
            FROM persons 
            left JOIN request ON persons.id = request.user_id;",

    '4' => $sql4 = "SELECT `id`, `name` FROM persons WHERE name LIKE '%а%';",

    '5' => "SELECT `id`, `name`, `birthday`
            FROM persons WHERE YEAR(birthday) >= 1991 AND YEAR(birthday) <= 1995;",
    '6' => "SELECT persons.id, persons.name from persons;",
);


if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $sql = $sql_array[$id];
    jsonResponse($db, $sql);
}

function jsonResponse($db, $sql) {
    mysqli_query($db,"SET NAMES utf8;");
    $response = array();
    $query = mysqli_query($db, $sql);
    while($queryset = $query->fetch_assoc()) {
        $response[] = $queryset;
    }
    echo json_encode($response);
}

?>



